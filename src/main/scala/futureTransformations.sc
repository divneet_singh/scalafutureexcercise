
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Random, Success, Try}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import java.lang.ArithmeticException
import  java.io.IOException

def add(x:Int,y:Int) = Future { x + y }

def div(a:Int,b:Int)=Future{a/b}

val f = Future{ Random.nextInt(100)}

val f2= Future{ math.max(10,20)}

//val future = Future(throw new Exception("Oops"))
//future.transform{
//  case Success(_) => Try("OK")
//      case Failure(_) => Try("KO")
//
//}

//Transformations "transform"
val test = div (4,8)

test.transform{
  case Success(_) => Try("Success")
  case Failure(_) => Try("Failure")
}

val test2 = div(5,0)
test2.transform{
  case Success(_) => Try("Success")
  case Failure(_) => Try("Failure")
}
//Transformations "transformWith "

// Transformations "collect"



//transformations "failed"

val r1 = div(4,0)

r1.failed

// transformations fallBackto
//It uses the first Future's value if it is successful or falls back to the second Future's value. However, if both the first and the second Future fails, then the error that is propagated to the caller is that of the first Future and not the second Future.


val randomVar = r1.fallbackTo(r1)

val randomVar1 = test2.fallbackTo(test)

//val futureFallingBackToAnotherErrorThrowingFuture: Future[Int] = futureCallingLegacyCode.fallbackTo (anotherErrorThrowingFuture)

//transformation "filter" for comprehension
val a = Future { 5 }
val g = a filter { _ % 2 == 1 }
Await.result(g, Duration.Zero)

//transformation "flatmap" & "map"
val test3 = add(1,2).flatMap(sum1 =>
  add(3,4).flatMap(sum2 =>
    add(5,6).map(sum3 => {
      println((sum1,sum2,sum3))
    })))
//(3,7,11)
test3

//transformation "flatten"

//How to flatten nested  Future in scala

val futureAdd = Future{add(2,3)}
futureAdd
//flatten future nesting



//transormation "mapTo"

//transformation "recover"
div(5,0) recover {
  case e :ArithmeticException => 0
}
//with flatten
val randomVar2 =div(4,2)recover {
  case e : ArithmeticException => 0
}

//alternate method
Future(5/0) recover{
  case e :ArithmeticException => 0
}

// transformation "recoverwith "

// error handling for future  just doesn't feel right though
//writing it here for the sake of it
val f3 =f recoverWith {
  case e :ArithmeticException => f2
}
div(6,0) recoverWith {
  case e : ArithmeticException => f2
}
// "with filter just feels weird right now will tackle it later


List("a", "b", "c").withFilter(_ == "b")
